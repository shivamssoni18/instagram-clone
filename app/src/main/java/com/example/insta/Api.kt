package com.example.insta

import retrofit2.Call
import retrofit2.http.GET

interface Api {

    @GET("list")
    fun getImageList(): Call<List<ImageDataPojo>>
}